﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class GestionFrais : Form
    {
        List<VisiteurFraisAutre> fraisToValid = Program.model.VisiteurFraisAutre.Where(e => e.Valide == false).OrderBy(e => e.DateFrais).ToList();

        public GestionFrais()
        {
            InitializeComponent();

            // dataGridView1 : Date | Visiteur | Libellé | Montant
            foreach (VisiteurFraisAutre frais in fraisToValid)
            {
                frais.VisiteVisiteur.GlobalUtilisateur.Custom = frais.VisiteVisiteur.GlobalUtilisateur.Prenom + " " + frais.VisiteVisiteur.GlobalUtilisateur.Nom.ToUpper();
                dataGridView1.Rows.Add(frais.DateFrais.ToString("d"), frais.VisiteVisiteur.GlobalUtilisateur.Custom, frais.Nom, frais.Montant, frais.Id);
            }
        }

        private void supprimer_FraisAutre(object sender, EventArgs e)
        {
            // Suppression frais autre
            foreach (DataGridViewRow ligne in dataGridView1.SelectedRows)
            {
                int id = (int)ligne.Cells["id_frais"].Value;
                VisiteurFraisAutre frais = Program.model.VisiteurFraisAutre.Where(f => f.Id == id).FirstOrDefault();
                Program.model.VisiteurFraisAutre.Remove(frais);

                if (Outils.SaveDatabase(this, "Suppression", false))
                    dataGridView1.Rows.Remove(ligne);
            }
        }

        private void valider_FraisAutre(object sender, EventArgs e)
        {
            // Validation frais autre
            foreach (DataGridViewRow ligne in dataGridView1.SelectedRows)
            {
                int id = (int)ligne.Cells["id_frais"].Value;
                VisiteurFraisAutre frais = Program.model.VisiteurFraisAutre.Where(f => f.Id == id).FirstOrDefault();
                frais.Valide = true;

                if (Outils.SaveDatabase(this, "Validation", false))
                    dataGridView1.Rows.Remove(ligne);
            }
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            // Affichage menu contextuel -> clic droit
            DataGridView.HitTestInfo info = dataGridView1.HitTest(e.X, e.Y);

            if (e.Button != MouseButtons.Right || info.RowIndex < 0 || info.ColumnIndex < 0)
                return;

            contextMenuStrip1.Show(dataGridView1, new Point(e.X, e.Y));
            dataGridView1.ClearSelection();
            dataGridView1.Rows[info.RowIndex].Selected = true;
        }

        public void form_Changed(object sender, EventArgs e)
        {
            // Réinitialisation des bouttons
            bool enabled = dataGridView1.SelectedRows.Count > 0;
            Outils.StyleButton(button1, enabled);
            Outils.StyleButton(button2, enabled);
        }
    }
}
