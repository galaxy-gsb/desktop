﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class AjoutFrais : Form
    {
        private VisiteurFraisForfaitaire fraisForf = new VisiteurFraisForfaitaire();
        private VisiteurFraisAutre fraisAutre = new VisiteurFraisAutre();

        public AjoutFrais()
        {
            InitializeComponent();

            // Bind comboBox
            BindingList<VisiteurFraisForfaitaireType> typesFrais = new BindingList<VisiteurFraisForfaitaireType>(Program.model.VisiteurFraisForfaitaireType.ToList());
            typeFraisForfait.DataSource = typesFrais;
            typeFraisForfait.DisplayMember = "Nom";

            // Initialisation frais forfaitaire par défaut
            fraisForf.Annee = DateTime.Now.Year;
            fraisForf.Mois = DateTime.Now.Month;
            fraisForf.Quantite = 1;
            fraisForf.VisiteurFraisForfaitaireType = typesFrais.First();

            // Bind controls frais forfaitaire
            typeFraisForfait.DataBindings.Add("SelectedItem", fraisForf, "VisiteurFraisForfaitaireType", true, DataSourceUpdateMode.OnPropertyChanged);
            quantiteFraisForfait.DataBindings.Add("Text", fraisForf, "Quantite", true, DataSourceUpdateMode.OnPropertyChanged);

            // Initialisation frais autre par défaut
            fraisAutre.Montant = 1;
            fraisAutre.DateFrais = DateTime.Now;
            fraisAutre.Valide = false;

            // Bind controls frais autres
            libelleFraisAutre.DataBindings.Add("Text", fraisAutre, "Nom", true, DataSourceUpdateMode.OnPropertyChanged);
            montantFraisAutre.DataBindings.Add("Text", fraisAutre, "Montant", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void buttonValideFraisForfait_Click(object sender, EventArgs e)
        {
            // Ajout/update frais forfaitaire au visiteur connecté
            VisiteurFraisForfaitaire fraisForfExiste = Program.utilisateur.VisiteVisiteur.VisiteurFraisForfaitaire.SingleOrDefault(f => f.Annee == DateTime.Now.Year && f.Mois == DateTime.Now.Month && f.FraisForfaitaireTypeId == fraisForf.VisiteurFraisForfaitaireType.Id);

            if (fraisForfExiste == null)
                Program.utilisateur.VisiteVisiteur.VisiteurFraisForfaitaire.Add(fraisForf);
            else
                fraisForfExiste.Quantite += fraisForf.Quantite;

            if (Outils.SaveDatabase(this, "Création"))
                Program.planning.update_Frais(null, null);

            Close();
        }

        private void buttonValideFraisAutre_Click(object sender, EventArgs e)
        {
            // Ajout frais autre au visiteur connecté
            Program.utilisateur.VisiteVisiteur.VisiteurFraisAutre.Add(fraisAutre);

            if (Outils.SaveDatabase(this, "Création"))
                Program.planning.update_Frais(null, null);

            Close();
        }

        private void fraisForfait_ValueChanged(object sender, EventArgs e)
        {
            // Réinitialisation des boutons frais forfaitaire
            bool enabled = quantiteFraisForfait.Value > 0 && Convert.ToDecimal(quantiteFraisForfait.Text) > 0;
            Outils.StyleButton(buttonValideFraisForfait, enabled);
        }

        private void fraisAutre_ValueChanged(object sender, EventArgs e)
        {
            // Réinitialisation des boutons frais autre
            bool enabled = libelleFraisAutre.Text != "" && montantFraisAutre.Value > 0 && Convert.ToDecimal(montantFraisAutre.Text) > 0;
            Outils.StyleButton(buttonValideFraisAutre, enabled);
        }
    }
}
