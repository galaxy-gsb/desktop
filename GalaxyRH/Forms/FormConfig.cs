﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class FormConfig : Form
    {
        private BindingList<VisiteurFraisForfaitaireType> typesFrais = new BindingList<VisiteurFraisForfaitaireType>(Program.model.VisiteurFraisForfaitaireType.ToList());
        private VisiteurFraisForfaitaireType typeFrais = new VisiteurFraisForfaitaireType();

        public FormConfig()
        {
            InitializeComponent();

            // Bind listBox
            listBox1.DataSource = typesFrais;
            listBox1.DisplayMember = "Nom";
            textBox1.DataBindings.Add("Text", typesFrais, "Nom", true, DataSourceUpdateMode.Never);
            numericUpDown2.DataBindings.Add("Value", typesFrais, "MontantUnitaire", true, DataSourceUpdateMode.Never);
            numericUpDown2.DataBindings.Add("Text", typesFrais, "MontantUnitaire", true, DataSourceUpdateMode.Never);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Ajout type frais forfaitaire
            typeFrais = new VisiteurFraisForfaitaireType();
            typeFrais.Nom = textBox1.Text;
            typeFrais.MontantUnitaire = numericUpDown2.Value;
            Program.model.VisiteurFraisForfaitaireType.Add(typeFrais);

            if (Outils.SaveDatabase(this, "Création"))
                typesFrais.Add(typeFrais);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Modification type frais forfaitaire
            typeFrais.Nom = textBox1.Text;
            typeFrais.MontantUnitaire = numericUpDown2.Value;

            if (Outils.SaveDatabase(this, "Modification"))
                typesFrais[typesFrais.IndexOf(typeFrais)] = typeFrais;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Suppression type frais forfaitaire
            Program.model.VisiteurFraisForfaitaireType.Remove(typeFrais);

            if (Outils.SaveDatabase(this, "Supression"))
                typesFrais.Remove(typeFrais);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Bind objet type frais
            typeFrais = ((VisiteurFraisForfaitaireType)listBox1.SelectedItem);
        }

        private void form_Changed(object sender, EventArgs e)
        {
            // Réinitialisation des boutons
            bool enabled = textBox1.Text != "" && numericUpDown2.Value > 0 && Convert.ToDecimal(numericUpDown2.Text) > 0;
            Outils.StyleButton(button1, enabled);
            Outils.StyleButton(button3, enabled);
            Outils.StyleButton(button2, enabled);
        }
    }
}
