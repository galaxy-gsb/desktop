﻿using System;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class AfficheNote : Form
    {
        public AfficheNote(Visite visite)
        {
            InitializeComponent();

            // Bind controls à la visite
            textBox1.DataBindings.Add("Text", visite, "SujetCommentaire", true, DataSourceUpdateMode.OnPropertyChanged);
            richTextBox1.DataBindings.Add("Text", visite, "Commentaire", true, DataSourceUpdateMode.OnPropertyChanged);

            bool isVisiteur = Program.utilisateur.GlobalTypeUtilisateur.Id == 2;
            button1.Visible = isVisiteur;
            textBox1.ReadOnly = !isVisiteur;
            richTextBox1.ReadOnly = !isVisiteur;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Modification note visite
            if (Outils.SaveDatabase(this, "Modification"))
                Close();
        }

        private void form_Changed(object sender, EventArgs e)
        {
            // Réinitialisation des bouttons
            bool enabled = richTextBox1.Text != "" && textBox1.Text != "";
            Outils.StyleButton(button1, enabled);
        }
    }
}
