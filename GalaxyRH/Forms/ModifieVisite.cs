﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class ModifieVisite : Form
    {
        private Visite visite = null;

        public ModifieVisite(Visite visitePost)
        {
            InitializeComponent();
            visite = visitePost;

            // Bind comboBox visiteurs
            List<VisiteVisiteur> visiteurs = new List<VisiteVisiteur>(Program.model.VisiteVisiteur.ToList());

            foreach (VisiteVisiteur visiteur in visiteurs)
                visiteur.Custom = visiteur.GlobalUtilisateur.Prenom + " " + visiteur.GlobalUtilisateur.Nom.ToUpper();

            listeVisiteurs.DataSource = visiteurs;
            listeVisiteurs.DisplayMember = "Custom";
            listeVisiteurs.DataBindings.Add("SelectedItem", visite, "VisiteVisiteur", true, DataSourceUpdateMode.OnPropertyChanged);

            // Bind comboBox praticiens
            List<VisitePraticien> praticiens = new List<VisitePraticien>(Program.model.VisitePraticien.ToList());
            listePraticiens.DataSource = praticiens;
            listePraticiens.DisplayMember = "Nom";
            listePraticiens.DataBindings.Add("SelectedItem", visite, "VisitePraticien", true, DataSourceUpdateMode.OnPropertyChanged);

            // Bind controls heure début et heure fin
            dateHeureDebut.DataBindings.Add("Value", visite, "DateDebut", true, DataSourceUpdateMode.OnPropertyChanged);
            dateHeureFin.DataBindings.Add("Value", visite, "DateFin", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Modification visite
            if (visite.DateDebut >= visite.DateFin)
            {
                MessageBox.Show("Les dates de votre visite ne sont pas possibles, merci de les vérifier.", "Date incorrecte", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (visite.VisiteVisiteur.Visite.Where(v => v.DateDebut <= visite.DateFin && v.DateFin >= visite.DateDebut && v.Id != visite.Id).Count() > 0)
            {
                MessageBox.Show("Une autre visite est déjà programmée sur ce créneau.", "Conflit avec une autre visite", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (Outils.SaveDatabase(this, "Modification"))
            {
                Program.planning.Visiteur = visite.VisiteVisiteur;
                Program.planning.update_Planning(null, null);
                Program.planning.changeVisiteurComboBox();
                Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
