﻿namespace GalaxyRH
{
    partial class AjoutFrais
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AjoutFrais));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.quantiteFraisForfait = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonValideFraisForfait = new System.Windows.Forms.Button();
            this.typeFraisForfait = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.libelleFraisAutre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.montantFraisAutre = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonValideFraisAutre = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantiteFraisForfait)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.montantFraisAutre)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(284, 119);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.quantiteFraisForfait);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.buttonValideFraisForfait);
            this.tabPage1.Controls.Add(this.typeFraisForfait);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(276, 93);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Frais forfaitaire";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Quantité :";
            // 
            // quantiteFraisForfait
            // 
            this.quantiteFraisForfait.Location = new System.Drawing.Point(87, 33);
            this.quantiteFraisForfait.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.quantiteFraisForfait.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quantiteFraisForfait.Name = "quantiteFraisForfait";
            this.quantiteFraisForfait.Size = new System.Drawing.Size(181, 20);
            this.quantiteFraisForfait.TabIndex = 3;
            this.quantiteFraisForfait.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.quantiteFraisForfait.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quantiteFraisForfait.TextChanged += new System.EventHandler(this.fraisForfait_ValueChanged);
            this.quantiteFraisForfait.ValueChanged += new System.EventHandler(this.fraisForfait_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Type de frais :";
            // 
            // buttonValideFraisForfait
            // 
            this.buttonValideFraisForfait.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonValideFraisForfait.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.buttonValideFraisForfait.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonValideFraisForfait.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.buttonValideFraisForfait.Location = new System.Drawing.Point(48, 60);
            this.buttonValideFraisForfait.Name = "buttonValideFraisForfait";
            this.buttonValideFraisForfait.Size = new System.Drawing.Size(181, 24);
            this.buttonValideFraisForfait.TabIndex = 1;
            this.buttonValideFraisForfait.Text = "Ajouter";
            this.buttonValideFraisForfait.UseVisualStyleBackColor = false;
            this.buttonValideFraisForfait.Click += new System.EventHandler(this.buttonValideFraisForfait_Click);
            // 
            // typeFraisForfait
            // 
            this.typeFraisForfait.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeFraisForfait.FormattingEnabled = true;
            this.typeFraisForfait.Location = new System.Drawing.Point(87, 6);
            this.typeFraisForfait.Name = "typeFraisForfait";
            this.typeFraisForfait.Size = new System.Drawing.Size(181, 21);
            this.typeFraisForfait.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.libelleFraisAutre);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.montantFraisAutre);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.buttonValideFraisAutre);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(276, 93);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Autre frais";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // libelleFraisAutre
            // 
            this.libelleFraisAutre.Location = new System.Drawing.Point(87, 6);
            this.libelleFraisAutre.Name = "libelleFraisAutre";
            this.libelleFraisAutre.Size = new System.Drawing.Size(181, 20);
            this.libelleFraisAutre.TabIndex = 10;
            this.libelleFraisAutre.TextChanged += new System.EventHandler(this.fraisAutre_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Montant :";
            // 
            // montantFraisAutre
            // 
            this.montantFraisAutre.DecimalPlaces = 2;
            this.montantFraisAutre.Location = new System.Drawing.Point(87, 33);
            this.montantFraisAutre.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.montantFraisAutre.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.montantFraisAutre.Name = "montantFraisAutre";
            this.montantFraisAutre.Size = new System.Drawing.Size(181, 20);
            this.montantFraisAutre.TabIndex = 8;
            this.montantFraisAutre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.montantFraisAutre.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.montantFraisAutre.TextChanged += new System.EventHandler(this.fraisAutre_ValueChanged);
            this.montantFraisAutre.ValueChanged += new System.EventHandler(this.fraisAutre_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Libellé :";
            // 
            // buttonValideFraisAutre
            // 
            this.buttonValideFraisAutre.BackColor = System.Drawing.Color.Silver;
            this.buttonValideFraisAutre.Enabled = false;
            this.buttonValideFraisAutre.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.buttonValideFraisAutre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonValideFraisAutre.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.buttonValideFraisAutre.Location = new System.Drawing.Point(48, 60);
            this.buttonValideFraisAutre.Name = "buttonValideFraisAutre";
            this.buttonValideFraisAutre.Size = new System.Drawing.Size(181, 24);
            this.buttonValideFraisAutre.TabIndex = 6;
            this.buttonValideFraisAutre.Text = "Ajouter";
            this.buttonValideFraisAutre.UseVisualStyleBackColor = false;
            this.buttonValideFraisAutre.Click += new System.EventHandler(this.buttonValideFraisAutre_Click);
            // 
            // AjoutFrais
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 119);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(300, 158);
            this.MinimumSize = new System.Drawing.Size(300, 158);
            this.Name = "AjoutFrais";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajout de frais";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantiteFraisForfait)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.montantFraisAutre)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown quantiteFraisForfait;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonValideFraisForfait;
        private System.Windows.Forms.ComboBox typeFraisForfait;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox libelleFraisAutre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown montantFraisAutre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonValideFraisAutre;
    }
}