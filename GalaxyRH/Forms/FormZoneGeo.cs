﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class FormZoneGeo : Form
    {
        private BindingList<GlobalRegion> regions = new BindingList<GlobalRegion>(Program.model.GlobalRegion.OrderBy(e => e.Nom).ToList());
        private GlobalRegion region = new GlobalRegion();
        private BindingList<GlobalRegionSecteur> secteurs = new BindingList<GlobalRegionSecteur>(Program.model.GlobalRegionSecteur.OrderBy(e => e.Nom).ToList());
        private GlobalRegionSecteur secteur = new GlobalRegionSecteur();

        public FormZoneGeo()
        {
            InitializeComponent();

            // Bind listBox régions
            listBox1.DataSource = regions;
            listBox1.DisplayMember = "Nom";

            // Bind controls nouvelle région
            textBox1.DataBindings.Add("Text", regions, "Id", true, DataSourceUpdateMode.Never);
            textBox2.DataBindings.Add("Text", regions, "Nom", true, DataSourceUpdateMode.Never);

            // Bind controls nouveau secteur
            listBox2.DataSource = secteurs;
            listBox2.DisplayMember = "Nom";
            textBox3.DataBindings.Add("Text", secteurs, "Nom", true, DataSourceUpdateMode.Never);
            comboBox2.DataBindings.Add("SelectedItem", secteurs, "GlobalUtilisateur", true, DataSourceUpdateMode.Never);

            // Bind comboBox responsables
            List<GlobalUtilisateur> utilisateurs = Program.model.GlobalUtilisateur.ToList();

            foreach (GlobalUtilisateur utilisateur in utilisateurs)
                utilisateur.Custom = utilisateur.Prenom + " " + utilisateur.Nom.ToUpper();

            comboBox2.DataSource = utilisateurs;
            comboBox2.DisplayMember = "Custom";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Ajout nouvelle région
            region = new GlobalRegion();
            region.Id = textBox1.Text;
            region.Nom = textBox2.Text;
            Program.model.GlobalRegion.Add(region);

            if (Outils.SaveDatabase(this, "Création"))
                regions.Add(region);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Modification région
            region.Nom = textBox2.Text;
            regions[regions.IndexOf(region)] = region;

            if (Outils.SaveDatabase(this, "Modification"))
                listBox1.SelectedItem = region;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            // Suppression région
            Program.model.GlobalRegion.Remove(region);

            if (Outils.SaveDatabase(this, "Suppression"))
                regions.Remove(region);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            // Ajout nouveau secteur
            secteur = new GlobalRegionSecteur();
            secteur.GlobalRegion = region;
            secteur.GlobalUtilisateur = ((GlobalUtilisateur)comboBox2.SelectedItem);
            secteur.Nom = textBox3.Text;
            region.GlobalRegionSecteur.Add(secteur);

            if (Outils.SaveDatabase(this, "Création"))
            {
                secteurs.Add(secteur);
                listBox2.SelectedItem = secteur;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Modification secteur
            secteur.Nom = textBox3.Text;
            secteur.GlobalUtilisateur = ((GlobalUtilisateur)comboBox2.SelectedItem);

            if (Outils.SaveDatabase(this, "Modification"))
                secteurs[secteurs.IndexOf(secteur)] = secteur;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Suppression secteur
            Program.model.GlobalRegionSecteur.Remove(secteur);

            if (Outils.SaveDatabase(this, "Suppression"))
                secteurs.Remove(secteur);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Bind listBox secteurs région sélectionnée
            region = (GlobalRegion)listBox1.SelectedItem;
            secteurs.Clear();

            if (region == null)
                return;

            foreach (GlobalRegionSecteur regionSecteur in region.GlobalRegionSecteur)
                secteurs.Add(regionSecteur);

            if (listBox2.Items.Count <= 0)
                return;

            secteur = region.GlobalRegionSecteur.First();
        }

        public void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Bind listBox secteur
            secteur = (GlobalRegionSecteur)listBox2.SelectedItem;
        }

        private void region_Changed(object sender, EventArgs e)
        {
            // Réinitialisation bouttons région
            bool enabled = textBox1.Text != "" && textBox2.Text != "";
            Outils.StyleButton(button1, enabled);
            Outils.StyleButton(button2, enabled);
            Outils.StyleButton(button5, enabled);
        }

        private void secteur_Changed(object sender, EventArgs e)
        {
            // Réinitialisation bouttons secteur
            bool enabled = textBox3.Text != "";
            Outils.StyleButton(button6, enabled);
            Outils.StyleButton(button4, enabled);
            Outils.StyleButton(button3, enabled);
        }
    }
}
