﻿namespace GalaxyRH
{
    partial class ModifieVisite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModifieVisite));
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateHeureFin = new System.Windows.Forms.DateTimePicker();
            this.dateHeureDebut = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.listePraticiens = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.listeVisiteurs = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Date de fin :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date de début :";
            // 
            // dateHeureFin
            // 
            this.dateHeureFin.CustomFormat = "dd/MM/yyyy H:mm";
            this.dateHeureFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateHeureFin.Location = new System.Drawing.Point(12, 158);
            this.dateHeureFin.Name = "dateHeureFin";
            this.dateHeureFin.Size = new System.Drawing.Size(212, 20);
            this.dateHeureFin.TabIndex = 8;
            // 
            // dateHeureDebut
            // 
            this.dateHeureDebut.CustomFormat = "dd/MM/yyyy H:mm";
            this.dateHeureDebut.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateHeureDebut.Location = new System.Drawing.Point(12, 112);
            this.dateHeureDebut.Name = "dateHeureDebut";
            this.dateHeureDebut.Size = new System.Drawing.Size(212, 20);
            this.dateHeureDebut.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Praticien";
            // 
            // listePraticiens
            // 
            this.listePraticiens.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listePraticiens.FormattingEnabled = true;
            this.listePraticiens.Location = new System.Drawing.Point(12, 66);
            this.listePraticiens.Name = "listePraticiens";
            this.listePraticiens.Size = new System.Drawing.Size(212, 21);
            this.listePraticiens.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Highlight;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.button1.Location = new System.Drawing.Point(12, 195);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 28);
            this.button1.TabIndex = 1;
            this.button1.Text = "Enregistrer";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listeVisiteurs
            // 
            this.listeVisiteurs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listeVisiteurs.FormattingEnabled = true;
            this.listeVisiteurs.Location = new System.Drawing.Point(12, 23);
            this.listeVisiteurs.Name = "listeVisiteurs";
            this.listeVisiteurs.Size = new System.Drawing.Size(212, 21);
            this.listeVisiteurs.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 15);
            this.label2.TabIndex = 12;
            this.label2.Text = "Visiteur";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Highlight;
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.button2.Location = new System.Drawing.Point(127, 195);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 28);
            this.button2.TabIndex = 13;
            this.button2.Text = "Annuler";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ModifieVisite
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(232, 229);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listeVisiteurs);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listePraticiens);
            this.Controls.Add(this.dateHeureFin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateHeureDebut);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(248, 267);
            this.MinimumSize = new System.Drawing.Size(248, 267);
            this.Name = "ModifieVisite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modifier";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateHeureFin;
        private System.Windows.Forms.DateTimePicker dateHeureDebut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox listePraticiens;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox listeVisiteurs;
        private System.Windows.Forms.Button button2;
    }
}