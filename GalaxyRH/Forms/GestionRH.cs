﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class GestionRH : Form
    {
        public GestionRH()
        {
            InitializeComponent();

            // Checkbox filtre types utilisateur
            List<GlobalTypeUtilisateur> types = Program.model.GlobalTypeUtilisateur.ToList();

            foreach (GlobalTypeUtilisateur type in types)
                checkedListBoxTypes.Items.Add(type, true);

            checkedListBoxTypes.ValueMember = "Id";
            checkedListBoxTypes.DisplayMember = "Nom";

            // Checkbox Specialités
            List<VisiteSpecialite> specialites = Program.model.VisiteSpecialite.ToList();

            foreach (VisiteSpecialite specialite in specialites)
                checkedListBox1.Items.Add(specialite, true);

            checkedListBox1.ValueMember = "Id";
            checkedListBox1.DisplayMember = "Nom";

            // DataGridView Employés : Id (caché) | Catégorie | Prenom | Nom | DateNaissance | Telephone | Email | Ville | CodePostal | Adresse1 | Adresse2
            List<GlobalUtilisateur> listeUtilisateurs = Program.model.GlobalUtilisateur.ToList();

            foreach (GlobalUtilisateur user in listeUtilisateurs)
                dataGridViewEmployes.Rows.Add(user.Id, user.GlobalTypeUtilisateur.Nom, user.Prenom, user.Nom, user.DateNaissance.ToShortDateString(), user.Telephone, user.Email, user.Ville, user.CodePostal, user.Adresse1, user.Adresse2);

            // DataGridView praticiens : Id (caché) | Nom | Adresse | Ville | CodePostal | OpinionSyndicale | Région
            List<VisitePraticien> listePraticiens = Program.model.VisitePraticien.ToList();

            foreach (VisitePraticien praticien in listePraticiens)
                dataGridView1.Rows.Add(praticien.Id, praticien.Nom, praticien.Adresse, praticien.Ville, praticien.CodePostal, praticien.CodePostal, praticien.OpinionSyndicale, praticien.GlobalRegionSecteur.GlobalRegion.Id + " - " + praticien.GlobalRegionSecteur.Nom);
        }

        private void ajout_Employe(object sender, EventArgs e)
        {
            // Ajout employé
            Outils.OuvrirForm(new AjoutPersonne(null));
        }

        private void modification_Employe(object sender, EventArgs e)
        {
            // Modification employé
            int id = (int)dataGridViewEmployes.SelectedRows[0].Cells[0].Value;
            GlobalUtilisateur user = Program.model.GlobalUtilisateur.Find(id);
            Outils.OuvrirForm(new AjoutPersonne(user));
        }

        private void supression_Employe(object sender, EventArgs e)
        {
            // Suppression employés
            if (dataGridViewEmployes.SelectedRows.Count == 0)
                return;

            DialogResult result = MessageBox.Show("Êtes-vous sûr de vouloir supprimer les lignes sélectionnées ?", "Confirmer suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (result != DialogResult.Yes)
                return;

            foreach (DataGridViewRow row in dataGridViewEmployes.SelectedRows)
            {
                int id = (int)row.Cells[0].Value;
                GlobalUtilisateur user = Program.model.GlobalUtilisateur.Find(id);
                Program.model.GlobalUtilisateur.Remove(user);

                if (Outils.SaveDatabase(this, "Suppression", false))
                    dataGridViewEmployes.Rows.RemoveAt(row.Index);
            }
        }

        private void dataGridViewEmployes_MouseClick(object sender, MouseEventArgs e)
        {
            // Afficher menu contextuel employés
            DataGridView.HitTestInfo info = dataGridViewEmployes.HitTest(e.X, e.Y);

            if (info.RowIndex < 0 || info.ColumnIndex < 0)
                return;

            int id = (int)dataGridViewEmployes.Rows[info.RowIndex].Cells[0].Value;

            if (e.Button != MouseButtons.Right)
                return;

            dataGridViewEmployes.ClearSelection();
            dataGridViewEmployes.Rows[info.RowIndex].Selected = true;
            contextMenuStrip1.Show(dataGridViewEmployes, new Point(e.X, e.Y));
        }

        private void dataGridViewEmployes_SelectionChanged(object sender, EventArgs e)
        {
            // Réinitialisation des bouttons
            bool enabled = dataGridViewEmployes.SelectedRows.Count > 0;
            Outils.StyleButton(button2, enabled);
            Outils.StyleButton(button3, enabled);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            // Ajout praticien
            Outils.OuvrirForm(new AjoutPraticien(null));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            // Modification praticien
            int id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            VisitePraticien praticien = Program.model.VisitePraticien.Find(id);
            Outils.OuvrirForm(new AjoutPraticien(praticien));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Suppression praticien
            if (dataGridView1.SelectedRows.Count == 0)
                return;

            DialogResult result = MessageBox.Show("Êtes-vous sûr de vouloir supprimer les lignes sélectionnées ?", "Confirmer suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (result != DialogResult.Yes)
                return;

            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                int id = (int)row.Cells[0].Value;
                VisitePraticien praticien = Program.model.VisitePraticien.Find(id);
                Program.model.VisitePraticien.Remove(praticien);

                if (Outils.SaveDatabase(this, "Suppression", false))
                    dataGridView1.Rows.RemoveAt(row.Index);
            }
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            // Afficher menu contextuel praticien
            DataGridView.HitTestInfo info = dataGridView1.HitTest(e.X, e.Y);

            if (info.RowIndex < 0 || info.ColumnIndex < 0)
                return;

            int id = (int)dataGridView1.Rows[info.RowIndex].Cells[0].Value;

            if (e.Button != MouseButtons.Right)
                return;

            dataGridView1.ClearSelection();
            dataGridView1.Rows[info.RowIndex].Selected = true;
            contextMenuStrip2.Show(dataGridView1, new Point(e.X, e.Y));
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            // Réinitialisation des bouttons
            bool enabled = dataGridView1.SelectedRows.Count > 0;
            Outils.StyleButton(button6, enabled);
            Outils.StyleButton(button4, enabled);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Cacher filtres
            panelOptions.Visible = !panelOptions.Visible;
            panel2.Visible = !panel2.Visible;
        }

        private void filtres_GrilleEmploye(object sender, EventArgs e)
        {
            // Filtrer grille employés
            List<GlobalUtilisateur> listeUtilisateurs = Program.model.GlobalUtilisateur.ToList();
            dataGridViewEmployes.Rows.Clear();

            foreach (GlobalUtilisateur user in listeUtilisateurs)
            {
                if (checkedListBoxTypes.CheckedItems.IndexOf(user.GlobalTypeUtilisateur) < 0)
                    continue;

                // Id (invisible) | Prenom | Nom | DateNaissance | Telephone | Email | Ville | CodePostal | Adresse1 | Adresse2
                dataGridViewEmployes.Rows.Add(user.Id, user.GlobalTypeUtilisateur.Nom, user.Prenom, user.Nom, user.DateNaissance.ToShortDateString(), user.Telephone, user.Email, user.Ville, user.CodePostal, user.Adresse1, user.Adresse2);
            }
        }

        private void filtres_GrillePraticiens(object sender, EventArgs e)
        {
            // Filtrer grille employés
            List<VisitePraticien> listePraticiens = Program.model.VisitePraticien.ToList();
            dataGridView1.Rows.Clear();

            foreach (VisitePraticien praticien in listePraticiens)
            {
                foreach (VisiteSpecialite specialite in praticien.VisiteSpecialite)
                {
                    if (checkedListBox1.CheckedItems.IndexOf(specialite) < 0)
                        continue;

                    dataGridView1.Rows.Add(praticien.Id, praticien.Nom, praticien.Adresse, praticien.Ville, praticien.CodePostal, praticien.CodePostal, praticien.OpinionSyndicale, praticien.GlobalRegionSecteur.GlobalRegion.Id + " - " + praticien.GlobalRegionSecteur.Nom);
                    break;
                }
            }
        }
    }
}