﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class PasswordRecovery : Form
    {
        private GlobalUtilisateur utilisateur = new GlobalUtilisateur();
        public PasswordRecovery(GlobalUtilisateur user)
        {
            InitializeComponent();

            if (user != null)
                utilisateur = user;

            textBox1.DataBindings.Add("Text", utilisateur, "Login", true, DataSourceUpdateMode.Never, "");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Renvoi des identifiants par e-mail
            Cursor = Cursors.WaitCursor;
            string login = textBox1.Text;
            utilisateur = Program.model.GlobalUtilisateur.SingleOrDefault(u => u.Login == login);

            if (utilisateur == null)
            {
                MessageBox.Show("Désolé, cet utilisateur n'est pas reconnu dans notre base de données. Veuillez vous rapprocher du service informatique ou ressources humaines.", "Identifiants incorrects", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor = Cursors.Default;
                return;
            }

            string password = Outils.CreatePassword(10);
            string subject = "Récupération de mot de passe";
            string body = "Vous avez récemment entamé une procédure de récupération de mot de passe sur l'application de gestion des ressources humaines de l'entreprise GSB.\r\n\r\nDans le cadre de cette procédure, nous vous communiquons vos nouveaux identifiants d'accès à l'application :\r\n\r\nUtilisateur : " + utilisateur.Login + "\r\nPassword : " + password;

            utilisateur.Password = Outils.EncodeMD5(password);
            Program.model.SaveChanges();
            Outils.SendMail(utilisateur, subject, body);
            MessageBox.Show("Vos nouveaux identifiants de connexion vous ont été envoyés à l'adresse " + utilisateur.Email + ".", "Identifiants envoyés", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Cursor = Cursors.Default;
            Close();
        }

        private void form_Changed(object sender, EventArgs e)
        {
            // Réinitialisation des buttons
            bool enabled = textBox1.Text != "";
            Outils.StyleButton(button1, enabled);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
