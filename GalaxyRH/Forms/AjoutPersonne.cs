﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class AjoutPersonne : Form
    {
        private GlobalUtilisateur user = null;

        public AjoutPersonne(GlobalUtilisateur utilisateur)
        {
            InitializeComponent();

            // Bind comboBox types utilisateur
            BindingList<GlobalTypeUtilisateur> typesUtilisateur = new BindingList<GlobalTypeUtilisateur>(Program.model.GlobalTypeUtilisateur.ToList());
            comboBox1.DataSource = typesUtilisateur;
            comboBox1.DisplayMember = "Nom";

            // Afficher / masquer controls ajout
            bool isAjout = utilisateur == null;
            button1.Visible = isAjout;
            button3.Visible = !isAjout;
            comboBox1.Enabled = isAjout;

            if (!isAjout)
                Text = "Modifier un collaborateur";

            // Initialisation utilisateur, valeurs par défaut si null
            user = utilisateur;

            if (user == null)
            {
                user = new GlobalUtilisateur();
                user.Login = "";
                user.Nom = "";
                user.Prenom = "";
                user.DateNaissance = DateTime.Now;
                user.Adresse1 = "";
                user.Adresse2 = "";
                user.Ville = "";
                user.CodePostal = "";
                user.Email = "";
                user.Telephone = "";
                user.Custom = "";
                user.GlobalTypeUtilisateur = typesUtilisateur.First();

                if (user.GlobalTypeUtilisateur.Id == 2)
                {
                    user.VisiteVisiteur = new VisiteVisiteur();
                    user.VisiteVisiteur.DateEmbauche = DateTime.Now;
                    user.VisiteVisiteur.DateFinContrat = DateTime.Now.AddYears(1);
                }
            }

            // Bind controls utilisateur
            textBox9.DataBindings.Add("Text", user, "Login", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox1.DataBindings.Add("Text", user, "Nom", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox2.DataBindings.Add("Text", user, "Prenom", true, DataSourceUpdateMode.OnPropertyChanged);
            dateTimePicker1.DataBindings.Add("Value", user, "DateNaissance", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox3.DataBindings.Add("Text", user, "Adresse1", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox4.DataBindings.Add("Text", user, "Adresse2", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox5.DataBindings.Add("Text", user, "Ville", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox6.DataBindings.Add("Text", user, "CodePostal", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox8.DataBindings.Add("Text", user, "Email", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox7.DataBindings.Add("Text", user, "Telephone", true, DataSourceUpdateMode.OnPropertyChanged);
            comboBox1.DataBindings.Add("SelectedItem", user, "GlobalTypeUtilisateur", true, DataSourceUpdateMode.OnPropertyChanged);

            if (user.GlobalTypeUtilisateur.Id == 2)
            {
                dateTimePicker2.DataBindings.Add("Value", user.VisiteVisiteur, "DateEmbauche", true, DataSourceUpdateMode.OnPropertyChanged);
                dateTimePicker3.DataBindings.Add("Value", user.VisiteVisiteur, "DateFinContrat", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Ajout utilisateur
            user.Password = textBox10.Text;

            if (!CheckUtilisateur(true))
                return;

            Program.model.GlobalUtilisateur.Add(user);
            Program.model.SaveChanges();
            Outils.SendMail(user, "Vos identifiants de connexion", "Vous avez été enregistré comme utilisateur de l'application de gestion des ressources humaines de l'entreprise GSB.\r\nVoici vos identifiants de connexion : \r\n\r\nLogin : " + user.Login + "\r\nPassword : " + textBox11.Text);
            MessageBox.Show("Le collaborateur a bien été enregistré dans notre base de données ! Un e-mail lui a été envoyé avec ses identifiants.", "Formulaire envoyé", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Modification utilisateur
            bool checkPassword = false;

            if (textBox10.Text != "")
            {
                user.Password = textBox10.Text;
                checkPassword = true;
            }

            if (!CheckUtilisateur(checkPassword))
                return;

            if (Outils.SaveDatabase(this, "Modification"))
                Close();
        }

        private bool CheckUtilisateur(bool checkPassword)
        {
            // Vérification formulaire utilisateur
            if (user.Password != textBox11.Text && checkPassword)
            {
                MessageBox.Show("Le deux mots de passe entrés ne sont pas identiques, veuillez les resaisir.", "Formulaire incorrect", MessageBoxButtons.OK, MessageBoxIcon.Error);
                user.Password = "";
                textBox10.Text = "";
                textBox11.Text = "";
                return false;
            }

            List<string> listeErreurs = UtilisateurCheckErrors(user);

            if (listeErreurs.Count > 0)
            {
                string messageErreur = "Des erreurs sont présentes dans la saisie de votre nouvel utilisateur. Champs incorrects :\r\n";

                foreach (string champsErreur in listeErreurs)
                    messageErreur += "\r\n - " + champsErreur;

                MessageBox.Show(messageErreur, "Formulaire incorrect", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (user.GlobalTypeUtilisateur.Id != 2)
                user.VisiteVisiteur = null;
            else if (user.VisiteVisiteur.DateEmbauche > user.VisiteVisiteur.DateFinContrat)
            {
                MessageBox.Show("La date de début de contrat du visiteur ne peut être supérieure à la date de fin de contrat.", "Formulaire incorrect", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            user.Password = Outils.EncodeMD5(user.Password);
            return true;
        }

        private List<string> UtilisateurCheckErrors(GlobalUtilisateur utilisateur)
        {
            // Vérification erreurs de saisie
            List<string> listeErreurs = new List<string>();

            if (utilisateur.Nom == null || utilisateur.Nom.Length < 2)
                listeErreurs.Add("Nom");
            if (utilisateur.Prenom == null || utilisateur.Prenom.Length < 2)
                listeErreurs.Add("Prénom");
            if (utilisateur.Adresse1 == null || utilisateur.Adresse1.Length < 2)
                listeErreurs.Add("Adresse");
            if (utilisateur.Ville == null || utilisateur.Ville.Length < 2)
                listeErreurs.Add("Ville");
            if (utilisateur.CodePostal == null || utilisateur.CodePostal.Length != 5)
                listeErreurs.Add("Code postal");
            if (utilisateur.Telephone == null || utilisateur.Telephone.Length != 10 || !Regex.IsMatch(utilisateur.Telephone, @"^\d+$"))
                listeErreurs.Add("Téléphone");
            if (utilisateur.Email == null || !Regex.IsMatch(utilisateur.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                listeErreurs.Add("Adresse e-mail");
            if (utilisateur.Login == null || utilisateur.Login.Length < 3)
                listeErreurs.Add("Login");
            if (utilisateur.Password == null || utilisateur.Password.Length < 2)
                listeErreurs.Add("Mot de passe");

            return listeErreurs;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Génération password aléatoire
            string password = Outils.CreatePassword(10);
            user.Password = password;
            textBox10.Text = password;
            textBox11.Text = password;
        }

        private void login_TextChanged(object sender, EventArgs e)
        {
            // Mise à jour login
            user.Login = (textBox1.Text.ToLower() + "." + textBox2.Text.ToLower()).Replace(" ", "-");
        }

        private void typeVisiteur_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Afficher / masquer controls visiteur
            bool isVisiteur = ((GlobalTypeUtilisateur)comboBox1.SelectedValue).Id == 2;
            label15.Visible = isVisiteur;
            label16.Visible = isVisiteur;
            dateTimePicker2.Visible = isVisiteur;
            dateTimePicker3.Visible = isVisiteur;
        }

        private void form_Changed(object sender, EventArgs e)
        {
            bool enabled = textBox1.Text.Length > 2
                && textBox2.Text.Length > 2
                && textBox3.Text.Length > 2
                && textBox5.Text.Length > 2
                && textBox6.Text.Length == 5
                && textBox7.Text.Length == 10 && Regex.IsMatch(textBox7.Text, @"^\d+$")
                && Regex.IsMatch(textBox8.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

            bool isVisiteur = ((GlobalTypeUtilisateur)comboBox1.SelectedValue).Id == 2;

            if (isVisiteur)
                enabled = enabled && dateTimePicker2.Value <= dateTimePicker3.Value;

            Outils.StyleButton(button1, enabled);
            Outils.StyleButton(button3, enabled);
        }
    }
}
