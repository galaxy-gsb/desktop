﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class AjoutPraticien : Form
    {
        private VisitePraticien praticien = new VisitePraticien();

        public AjoutPraticien(VisitePraticien prat)
        {
            InitializeComponent();

            // Initialisation comboBox secteurs;
            List<GlobalRegionSecteur> secteurs = Program.model.GlobalRegionSecteur.OrderBy(s => s.GlobalRegion.Id).ToList();

            foreach (GlobalRegionSecteur secteur in secteurs)
                secteur.Custom = secteur.GlobalRegion.Id + " - " + secteur.Nom;

            comboBox1.DataSource = secteurs;
            comboBox1.DisplayMember = "Custom";

            // Initialisation praticien
            praticien = prat;

            // Initialisation bouttons
            bool enabled = prat == null;
            button1.Visible = !enabled;
            button3.Visible = enabled;

            if (praticien == null)
            {
                praticien = new VisitePraticien();
                praticien.Nom = "";
                praticien.Adresse = "";
                praticien.Ville = "";
                praticien.CodePostal = "";
                praticien.CoefficientNotoriete = 1.00M;
                praticien.OpinionSyndicale = "";
                praticien.GlobalRegionSecteur = secteurs.First();
            }

            // Bind controls praticien
            textBox1.DataBindings.Add("Text", praticien, "Nom", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox2.DataBindings.Add("Text", praticien, "Adresse", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox3.DataBindings.Add("Text", praticien, "Ville", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox4.DataBindings.Add("Text", praticien, "CodePostal", true, DataSourceUpdateMode.OnPropertyChanged);
            numericUpDown1.DataBindings.Add("Text", praticien, "CoefficientNotoriete", true, DataSourceUpdateMode.OnPropertyChanged);
            richTextBox1.DataBindings.Add("Text", praticien, "OpinionSyndicale", true, DataSourceUpdateMode.OnPropertyChanged);
            comboBox1.DataBindings.Add("SelectedItem", praticien, "GlobalRegionSecteur", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Ajout nouveau praticien
            Program.model.VisitePraticien.Add(praticien);
            Outils.SaveDatabase(this, "Création");
            Close();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            // Modification praticien existant
            Outils.SaveDatabase(this, "Modification");
            Close();
        }

        private void form_TextChanged(object sender, EventArgs e)
        {
            // Réinitialisation des bouttons
            bool enabled = textBox1.Text.Length > 1 && textBox2.Text.Length > 1 && textBox3.Text.Length > 1 && textBox4.Text.Length == 5 && Convert.ToDecimal(numericUpDown1.Text) > 0;
            Outils.StyleButton(button3, enabled);
            Outils.StyleButton(button1, enabled);
        }
    }
}
