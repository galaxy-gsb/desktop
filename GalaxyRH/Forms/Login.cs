﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class Login : Form
    {
        GlobalUtilisateur utilisateur = new GlobalUtilisateur();

        public Login(GlobalUtilisateur user)
        {
            InitializeComponent();
            utilisateur = user;

            if (utilisateur == null)
                utilisateur = Program.model.GlobalUtilisateur.Find(103);

            textBox1.DataBindings.Add("Text", utilisateur, "Login", true, DataSourceUpdateMode.Never);
            textBox3.Text = "test123";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Connexion
            Cursor = Cursors.WaitCursor;
            labelErreur.Visible = false;
            string login = textBox1.Text;
            string password = Outils.EncodeMD5(textBox3.Text);
            utilisateur = Program.model.GlobalUtilisateur.SingleOrDefault(u => u.Login == login && u.Password == password);

            if (utilisateur == null)
            {
                labelErreur.Visible = true;
                Cursor = Cursors.Default;
                return;
            }

            Program.utilisateur = utilisateur;

            if (utilisateur.GlobalTypeUtilisateur.Id == 1 || utilisateur.GlobalTypeUtilisateur.Id == 4)
                Outils.OuvrirForm(new InterfaceRH());
            else
                Outils.OuvrirForm(new Planning());

            Cursor = Cursors.Default;
            Hide();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Récupération de mot de passe
            Outils.OuvrirForm(new PasswordRecovery(utilisateur));
        }

        private void form_Changed(object sender, EventArgs e)
        {
            // Réinitialisation des bouttons
            bool enabled = textBox1.Text != "" && textBox3.Text != "";
            Outils.StyleButton(button1, enabled);
        }
    }
}
