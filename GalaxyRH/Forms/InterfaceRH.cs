﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;

namespace GalaxyRH
{
    public partial class InterfaceRH : Form
    {
        public InterfaceRH()
        {
            InitializeComponent();

            // Statistiques : meilleurs visiteurs
            List<VisiteVisiteur> visiteurs = Program.model.VisiteVisiteur.Where(e => e.Visite.Count > 0).OrderByDescending(e => e.Visite.Count).Take(5).ToList();
            visiteurs.Reverse();
            Series series = visiteursPlusActifs.Series[0];

            for (int i = 0; i < visiteurs.Count; i++)
            {
                visiteurs[i].GlobalUtilisateur.Custom = visiteurs[i].GlobalUtilisateur.Prenom + " " + visiteurs[i].GlobalUtilisateur.Nom.ToUpper();
                series.Points.Add(visiteurs[i].Visite.Count);
                series.Points[i].Label = visiteurs[i].Visite.Count + " visites";
                series.Points[i].LegendText = visiteurs[i].GlobalUtilisateur.Custom;
                series.Points[i].ToolTip = visiteurs[i].GlobalUtilisateur.Custom;
                series.Points[i].LegendToolTip = visiteurs[i].GlobalUtilisateur.Custom + " - " + visiteurs[i].Visite.Count + " visites";
            }

            visiteursPlusActifs.PaletteCustomColors = new Color[] { Color.FromArgb(100, 100, 100), Color.FromArgb(193, 0, 0), Color.White, Color.FromArgb(0, 149, 255), Color.Gold };

            // Statistiques : frais du mois
            Series seriesF = chartFrais.Series[0];
            IQueryable<VisiteurFraisAutre> listeFraisAutres = Program.model.VisiteurFraisAutre.Where(f => f.DateFrais.Month == DateTime.Now.Month && f.DateFrais.Year == DateTime.Now.Year && f.Valide == true);
            double nbFraisAutre = listeFraisAutres.Count() > 0 ? Convert.ToDouble(listeFraisAutres.Sum(f => f.Montant)) : 0;

            if (nbFraisAutre > 0)
            {
                DataPoint pointTemp = seriesF.Points.Add(nbFraisAutre);
                pointTemp.Label = nbFraisAutre + " €";
                pointTemp.LegendText = "Autres frais";
                pointTemp.ToolTip = "Autres frais";
                pointTemp.LegendToolTip = "Autres frais : " + nbFraisAutre + " €";
            }
            
            foreach (VisiteurFraisForfaitaireType forfaitType in Program.model.VisiteurFraisForfaitaireType.ToList())
            {
                IQueryable<VisiteurFraisForfaitaire> listeFraisForfaitaires = Program.model.VisiteurFraisForfaitaire.Where(f => f.FraisForfaitaireTypeId == forfaitType.Id && f.Mois == DateTime.Now.Month && f.Annee == DateTime.Now.Year);
                double nbFraisForfait = listeFraisForfaitaires.Count() > 0 ? Convert.ToDouble(listeFraisForfaitaires.Sum(f => f.Quantite * f.VisiteurFraisForfaitaireType.MontantUnitaire)) : 0;

                if (nbFraisForfait <= 0)
                    continue;

                DataPoint pointTemp = seriesF.Points.Add(nbFraisForfait);
                pointTemp.Label = nbFraisForfait + " €";
                pointTemp.LegendText = forfaitType.Nom;
                pointTemp.ToolTip = forfaitType.Nom;
                pointTemp.LegendToolTip = forfaitType.Nom + " : " + nbFraisForfait + " €";

            }

            // Prochaines visites : Date | Praticien | Visiteur | Adresse
            List<Visite> visites = Program.model.Visite.Where(v => v.DateDebut > DateTime.Now).OrderBy(v => v.DateDebut).Take(10).ToList();

            foreach (Visite visite in visites)
            {
                visite.VisiteVisiteur.GlobalUtilisateur.Custom = visite.VisiteVisiteur.GlobalUtilisateur.Prenom + " " + visite.VisiteVisiteur.GlobalUtilisateur.Nom.ToUpper();
                tableauProchainesVisites.Rows.Add(visite.DateDebut.ToShortDateString(), visite.VisitePraticien.Nom, visite.VisiteVisiteur.GlobalUtilisateur.Custom, visite.VisitePraticien.Adresse + " - " + visite.VisitePraticien.CodePostal + " " + visite.VisitePraticien.Ville);
            }

            // Dernières visites : Date | Praticien | Visiteur | Note | ID
            List<Visite> visitesLast = Program.model.Visite.Where(v => v.DateDebut < DateTime.Now).OrderByDescending(v => v.DateDebut).Take(10).ToList();

            foreach (Visite visiteLast in visitesLast)
            {
                Bitmap tempNote = Properties.Resources.croix;

                if (visiteLast.Commentaire != "")
                    tempNote = Properties.Resources.note;

                visiteLast.VisiteVisiteur.GlobalUtilisateur.Custom = visiteLast.VisiteVisiteur.GlobalUtilisateur.Prenom + " " + visiteLast.VisiteVisiteur.GlobalUtilisateur.Nom.ToUpper();
                tableauDernieresVisites.Rows.Add(visiteLast.DateDebut.ToShortDateString(), visiteLast.VisitePraticien.Nom, visiteLast.VisiteVisiteur.GlobalUtilisateur.Custom, tempNote, visiteLast.Id);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            // Menu form Planning
            Outils.OuvrirForm(new Planning());
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            // Menu form GestionFrais
            Outils.OuvrirForm(new GestionFrais());
        }

        private void configurerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Menu form FormConfig
            Outils.OuvrirForm(new FormConfig());
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            // Menu form FormZoneGeo
            Outils.OuvrirForm(new FormZoneGeo());
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            // Menu form GestionRH
            Outils.OuvrirForm(new GestionRH());
        }

        private void aProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ouverture crédits application
            string textMessage = "Galaxy RH - Version 2.1";
            textMessage += "\nDéveloppé par Kevin Konrad, Sosthèn Gaillard, Luc Gopalakrisna, Valentin Durand";
            MessageBox.Show(textMessage, "À propos de l'application", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void aideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ouverture dossier PDF
            string Path = Application.ExecutablePath;
            string vers = "bin\\Debug\\GalaxyRH.EXE";
            Path = Path.Replace(vers, "");
            Path = Path.Replace("\\", "/") + "Resources/documentation.pdf";
            System.Diagnostics.ProcessStartInfo pdf = new System.Diagnostics.ProcessStartInfo(@Path, "");
            System.Diagnostics.Process.Start(pdf);
        }

        private void tableauDernieresVisites_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // Affichage note de visite
            if (e.ColumnIndex != 3)
                return;

            int visiteId = Convert.ToInt32(tableauDernieresVisites.Rows[e.RowIndex].Cells["id"].Value);
            Visite visite = Program.model.Visite.Find(visiteId);
            Outils.OuvrirForm(new AfficheNote(visite));
        }

        private void InterfaceRH_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Outils.OuvrirForm(new Login(Program.utilisateur));
        }

        private void fermerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
