﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GalaxyRH
{
    public partial class Planning : Form
    {
        private Visite visite = new Visite();
        private static List<VisiteVisiteur> visiteurs = Program.model.VisiteVisiteur.ToList();
        private VisiteVisiteur visiteur = null;

        public VisiteVisiteur Visiteur
        {
            get { return visiteur; }
            set { visiteur = value; }
        }

        public Planning()
        {
            InitializeComponent();

            // Initialisation visiteur
            Visiteur = visiteurs.SingleOrDefault(v => v.UtilisateurId == Program.utilisateur.Id);

            if (Visiteur != null)
            {
                comboBox1.Enabled = false;
                groupBox1.Visible = false;
                panel4.Visible = false;
                button4.Visible = true;
            }
            else
                Visiteur = visiteurs.First();

            // Liste déroulante visiteurs
            comboBox1.DataSource = visiteurs;
            comboBox1.DisplayMember = "Custom";
            comboBox1.DataBindings.Add("SelectedItem", this, "Visiteur", true, DataSourceUpdateMode.OnPropertyChanged);

            // Initilisation visite
            initVisite();

            // Liste déroulante praticiens
            List<VisitePraticien> praticiens = Program.model.VisitePraticien.ToList();
            comboBox2.DataSource = praticiens;
            comboBox2.DisplayMember = "Nom";

            // Initialisation des horaires manuelles
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = dateTimePicker1.Value.AddHours(1);
        }

        private void initVisite()
        {
            visite = new Visite();
            visite.DateDebut = DateTime.Now;
            visite.DateFin = DateTime.Now.AddHours(1);
            visite.SujetCommentaire = "";
            visite.Commentaire = "";
            visite.VisiteVisiteur = Visiteur;
            visite.VisitePraticien = Program.model.VisitePraticien.First();

            // Bind controls nouvelle visite
            comboBox2.DataBindings.Clear();
            comboBox2.DataBindings.Add("SelectedItem", visite, "VisitePraticien", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void changeVisiteurComboBox()
        {
            comboBox1.SelectedItem = visiteurs.Where(v => v.UtilisateurId == Visiteur.UtilisateurId).FirstOrDefault();
        }

        public void update_Visiteur(object sender, EventArgs e)
        {
            visite.VisiteVisiteur = Visiteur;
            monthCalendar1.SelectionStart = DateTime.Now;
            monthCalendar1.SelectionEnd = DateTime.Now.AddDays(1);
            update_Planning(null, null);
        }

        public void update_Planning(object sender, EventArgs e)
        {
            // dataGridView1 : DateDebut | DateFin | Visiteur | Praticien | Adresse | IdVisite
            List<Visite> visites = Visiteur.Visite.Where(v => v.DateDebut.Year == monthCalendar1.SelectionRange.Start.Year && v.DateDebut.Month == monthCalendar1.SelectionRange.Start.Month && v.DateDebut.Day == monthCalendar1.SelectionRange.Start.Day).OrderBy(v => v.DateDebut).ToList();
            dataGridView1.Rows.Clear();

            foreach (Visite visite in visites)
                dataGridView1.Rows.Add(visite.DateDebut.ToString("g"), visite.DateFin.ToString("g"), visite.VisitePraticien.Nom, visite.VisitePraticien.Adresse + " - " + visite.VisitePraticien.CodePostal + " " + visite.VisitePraticien.Ville, visite.Id);

            update_Frais(null, null);
        }

        public void update_Frais(object sender, EventArgs e)
        {
            // Mise à jour frais mois et frais journée
            decimal totalFraisAutresDeJour = Visiteur.VisiteurFraisAutre.Where(f => f.DateFrais.Year == monthCalendar1.SelectionRange.Start.Year && f.DateFrais.Month == monthCalendar1.SelectionRange.Start.Month && f.DateFrais.Day == monthCalendar1.SelectionRange.Start.Day && f.Valide).Sum(f => f.Montant);
            decimal totalFraisAutresDeMois = Visiteur.VisiteurFraisAutre.Where(f => f.DateFrais.Year == monthCalendar1.SelectionRange.Start.Year && f.DateFrais.Month == monthCalendar1.SelectionRange.Start.Month && f.Valide).Sum(f => f.Montant);
            decimal totalFraisForfaitairesDeMois = Visiteur.VisiteurFraisForfaitaire.Where(f => f.Annee == monthCalendar1.SelectionRange.Start.Year && f.Mois == monthCalendar1.SelectionRange.Start.Month).Sum(f => (f.VisiteurFraisForfaitaireType.MontantUnitaire * f.Quantite));

            textBox2.Text = totalFraisAutresDeJour.ToString("F") + " €";
            textBox1.Text = (totalFraisAutresDeMois + totalFraisForfaitairesDeMois).ToString("F") + " €";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string messageErreur = "";

            // On initialise la date du debut et de la fin de la visite
            DateTime debutVisite = new DateTime();
            DateTime finVisite = debutVisite;

            // Mode manuel
            if (radioButton2.Checked)
            {
                debutVisite = monthCalendar1.SelectionStart.Date + dateTimePicker1.Value.TimeOfDay;
                finVisite = monthCalendar1.SelectionStart.Date + dateTimePicker2.Value.TimeOfDay;
            }
            // Mode automatique
            else
            {
                // On récupère la dernière visite de la journée et si elle existe, la nouvelle visite commence une heure plus tard
                List<Visite> visitesDeLaJournee = Visiteur.Visite.Where(v => v.DateDebut.Date == monthCalendar1.SelectionStart.Date).OrderByDescending(v => v.DateDebut).ToList();
                if (visitesDeLaJournee.Count > 0)
                {
                    debutVisite = visitesDeLaJournee[0].DateFin.AddHours(1);
                }
                else
                {
                    // Si pas de visite, on commence à 8h
                    debutVisite = monthCalendar1.SelectionStart.AddHours(8);
                }

                // Si c'est pas bon, on va chercher un créneau d'une heure dispo dans la journée
                if (debutVisite.Date != monthCalendar1.SelectionStart.Date || debutVisite.Hour > 20 || (debutVisite.Date == DateTime.Now.Date && debutVisite.TimeOfDay < DateTime.Now.TimeOfDay))
                {
                    DateTime heureDebutTemp = monthCalendar1.SelectionStart.AddHours(8);
                    bool chercher = true;
                    while (heureDebutTemp.Hour < 20 && chercher == true)
                    {
                        if ((visitesDeLaJournee.Where(v => v.DateDebut <= heureDebutTemp.AddHours(1) && v.DateFin >= heureDebutTemp).Count() == 0) && 
                            ((heureDebutTemp.Date == DateTime.Now.Date && heureDebutTemp.TimeOfDay >= DateTime.Now.TimeOfDay) || (heureDebutTemp.Date != DateTime.Now.Date)))
                            chercher = false;
                        else
                            heureDebutTemp = heureDebutTemp.AddHours(1);
                    }
                    if (heureDebutTemp.Hour >= 20 && visitesDeLaJournee.Where(v => v.DateDebut <= heureDebutTemp.AddHours(1) && v.DateFin >= heureDebutTemp).Count() > 0)
                        messageErreur = "Aucun créneau disponible pour le mode automatique, utilisez le mode manuel.";
                    else
                        debutVisite = heureDebutTemp;
                }

                // Fin une heure plus tard
                finVisite = debutVisite.AddHours(1);
            }
            
            // On vérifie qu'une autre visite n'existe pas sur le créneau
            int visiteSurCreneau = Visiteur.Visite.Where(v => v.DateDebut <= finVisite && v.DateFin >= debutVisite).Count();

            if (visiteSurCreneau > 0)
                messageErreur = "Une autre visite existe déjà sur ce créneau.";

            // On vérifie que les horaires sont cohérentes
            bool visiteIncoherente = debutVisite.TimeOfDay >= finVisite.TimeOfDay
                || debutVisite.Date != monthCalendar1.SelectionStart.Date 
                || finVisite.Date != monthCalendar1.SelectionStart.Date 
                || debutVisite <= DateTime.Now;

            if (messageErreur == "" && visiteIncoherente)
            {
                messageErreur = "Les horaires de la visite sont incohérentes.";
            }

            if (messageErreur != "")
            {
                MessageBox.Show(messageErreur, "Erreur lors de l'ajout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                initVisite();
            }
            else
            {
                // Ajout nouvelle visite
                visite.DateDebut = debutVisite;
                visite.DateFin = finVisite;
                Visiteur.Visite.Add(visite);
                initVisite();

                if (Outils.SaveDatabase(this, "Création"))
                    update_Planning(null, null);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Modification visite
            Visite visiteModif = Program.model.Visite.Find(Convert.ToInt32(dataGridView1.CurrentRow.Cells["id_visite"].Value));
            Outils.OuvrirForm(new ModifieVisite(visiteModif));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Suppression visite
            DialogResult result = MessageBox.Show("Voulez-vous vraiment supprimer cette visite ?\n\nCette action est définitive.", "Confirmation de suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result != DialogResult.Yes)
                return;

            Visite visiteSupp = Program.model.Visite.Find(Convert.ToInt32(dataGridView1.CurrentRow.Cells["id_visite"].Value));
            Program.model.Visite.Remove(visiteSupp);

            if (Outils.SaveDatabase(this, "Suppression"))
                update_Planning(null, null);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // Form affiche note
            Visite visiteNote = Program.model.Visite.Find(Convert.ToInt32(dataGridView1.CurrentRow.Cells["id_visite"].Value));
            Outils.OuvrirForm(new AfficheNote(visiteNote));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Form ajout frais
            Outils.OuvrirForm(new AjoutFrais());
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            // Afficher menu contextuel employés
            DataGridView.HitTestInfo info = dataGridView1.HitTest(e.X, e.Y);

            if (info.RowIndex < 0 || info.ColumnIndex < 0)
                return;

            int id = (int)dataGridView1.Rows[info.RowIndex].Cells["id_visite"].Value;

            if (e.Button != MouseButtons.Right)
                return;

            dataGridView1.ClearSelection();
            dataGridView1.Rows[info.RowIndex].Selected = true;
            contextMenuStrip1.Show(dataGridView1, new Point(e.X, e.Y));
        }

        private void Planning_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Affichage du form login si utilisateur connecté est visiteur
            if (Program.utilisateur.GlobalTypeUtilisateur.Id == 2)
                Outils.OuvrirForm(new Login(Program.utilisateur));
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            // Enable / disable datePickers si visite automatique
            dateTimePicker1.Enabled = radioButton2.Checked;
            dateTimePicker2.Enabled = radioButton2.Checked;
        }

        public void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            // Réinitialisation des bouttons grille visite
            bool enabled = dataGridView1.SelectedRows.Count > 0;
            Outils.StyleButton(button2, enabled);
            Outils.StyleButton(button3, enabled);
        }

        private void dateTimePickers_ValueChanged(object sender, EventArgs e)
        {
            // Initilisation des bouttons nouvelle visite
            bool enabled = dateTimePicker1.Value.TimeOfDay < dateTimePicker2.Value.TimeOfDay && monthCalendar1.SelectionStart.Date >= DateTime.Now.Date;
            Outils.StyleButton(button1, enabled);
        }
    }
}
