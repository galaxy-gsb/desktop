﻿using System;
using System.Windows.Forms;

namespace GalaxyRH
{
    static class Program
    {
        // Initialisation model Entity
        public static gsb3Entities model = new gsb3Entities();

        // Initialisation utilisateur du programme
        public static GlobalUtilisateur utilisateur = new GlobalUtilisateur();

        // Initialisation Forms uniques du programme
        public static AfficheNote afficheNote;
        public static AjoutFrais ajoutFrais;
        public static AjoutPersonne ajoutPersonne;
        public static AjoutPraticien ajoutPraticien;
        public static FormConfig formConfig;
        public static FormZoneGeo formZoneGeo;
        public static GestionFrais gestionFrais;
        public static GestionRH gestionRH;
        public static InterfaceRH interfaceRH;
        public static Login login;
        public static ModifieVisite modifieVisite;
        public static PasswordRecovery passwordRecovery;
        public static Planning planning;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            login = new Login(null);
            Application.Run(login);
        }
    }
}
