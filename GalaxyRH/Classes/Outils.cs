﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace GalaxyRH
{
    class Outils
    {
        public static string EncodeMD5(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hashBytes.Length; i++)
                sb.Append(hashBytes[i].ToString("X2"));

            return sb.ToString();
        }

        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder sb = new StringBuilder();
            Random rnd = new Random();

            while (0 < length--)
                sb.Append(valid[rnd.Next(valid.Length)]);

            return sb.ToString();
        }

        public static void SendMail(GlobalUtilisateur receiver, string subject, string body)
        {
            MailAddress from = new MailAddress("kevin.konrad@edu.itescia.fr", "GSB - Service RH");
            MailAddress to = new MailAddress(receiver.Email, receiver.Prenom + " " + receiver.Nom);
            MailMessage message = new MailMessage(from, to);
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

            message.Body = "Bonjour " + receiver.Prenom + ",\r\n\r\n" + body + "\r\n\r\nLe service Ressources Humaines de GSB.";
            message.Subject = subject;

            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("kevin.konrad@edu.itescia.fr", "cdy16m6n");
            smtp.EnableSsl = true;
            smtp.Send(message);
        }

        public static void StyleButton(Button button, bool enabled)
        {
            button.Enabled = enabled;

            if (enabled)
            {
                button.ForeColor = SystemColors.HighlightText;
                button.BackColor = SystemColors.Highlight;
                button.FlatAppearance.BorderColor = SystemColors.Highlight;
            }
            else
            {
                button.ForeColor = SystemColors.ControlDarkDark;
                button.BackColor = Color.Silver;
                button.FlatAppearance.BorderColor = Color.Silver;
            }
        }

        public static bool SaveDatabase(Form form, string action, bool afficherMessage = true)
        {
            form.Cursor = Cursors.WaitCursor;

            try
            {
                Program.model.SaveChanges();

                if (afficherMessage)
                    MessageBox.Show(action + " réussie.", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            catch (Exception ex)
            {
                RejectChanges();
                MessageBox.Show("Une erreur est survenue lors de la " + action.ToLower() + ". Veuillez réessayer ultérieurement.\r\n\r\nSource : " + ex.Source + "\r\n\r\n Pile d'appel :\r\n" + ex.StackTrace, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                form.Cursor = Cursors.Default;
                return false; 
            }

            form.Cursor = Cursors.Default;
            return true;
        }

        public static void RejectChanges()
        {
            foreach (var entry in Program.model.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        entry.Reload();
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        entry.Reload();
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                }
            }
        }

        public static void OuvrirForm(Form form)
        {
            // AfficheNote
            if (form is AfficheNote)
            {
                if (Program.afficheNote != null)
                    Program.afficheNote.Close();

                Program.afficheNote = (AfficheNote)form;
                Program.afficheNote.Show();
            }
            // AjoutFrais
            else if (form is AjoutFrais)
            {
                if (Program.ajoutFrais != null)
                    Program.ajoutFrais.Close();

                Program.ajoutFrais = (AjoutFrais)form;
                Program.ajoutFrais.Show();
            }
            // AjoutPersonne
            else if (form is AjoutPersonne)
            {
                if (Program.ajoutPersonne != null)
                    Program.ajoutPersonne.Close();

                Program.ajoutPersonne = (AjoutPersonne)form;
                Program.ajoutPersonne.Show();
            }
            // AjoutPraticien
            else if (form is AjoutPraticien)
            {
                if (Program.ajoutPraticien != null)
                    Program.ajoutPraticien.Close();

                Program.ajoutPraticien = (AjoutPraticien)form;
                Program.ajoutPraticien.Show();
            }
            // FormConfig
            else if (form is FormConfig)
            {
                if (Program.formConfig != null)
                    Program.formConfig.Close();

                Program.formConfig = (FormConfig)form;
                Program.formConfig.Show();
            }
            // FormZoneGeo
            else if (form is FormZoneGeo)
            {
                if (Program.formZoneGeo != null)
                    Program.formZoneGeo.Close();

                Program.formZoneGeo = (FormZoneGeo)form;
                Program.formZoneGeo.Show();
            }
            // GestionFrais
            else if (form is GestionFrais)
            {
                if (Program.gestionFrais != null)
                    Program.gestionFrais.Close();

                Program.gestionFrais = (GestionFrais)form;
                Program.gestionFrais.Show();
            }
            // GestionRH
            else if (form is GestionRH)
            {
                if (Program.gestionRH != null)
                    Program.gestionRH.Close();

                Program.gestionRH = (GestionRH)form;
                Program.gestionRH.Show();
            }
            // InterfaceRH
            else if (form is InterfaceRH)
            {
                if (Program.interfaceRH != null)
                    Program.interfaceRH.Close();

                Program.interfaceRH = (InterfaceRH)form;
                Program.interfaceRH.Show();
            }
            // Login
            else if (form is Login)
            {
                Program.login.Show();
            }
            // ModifieVisite
            else if (form is ModifieVisite)
            {
                if (Program.modifieVisite != null)
                    Program.modifieVisite.Close();

                Program.modifieVisite = (ModifieVisite)form;
                Program.modifieVisite.Show();
            }
            // PasswordRecovery
            else if (form is PasswordRecovery)
            {
                if (Program.passwordRecovery != null)
                    Program.passwordRecovery.Close();

                Program.passwordRecovery = (PasswordRecovery)form;
                Program.passwordRecovery.Show();
            }
            // Planning
            else if (form is Planning)
            {
                if (Program.planning != null)
                    Program.planning.Close();

                Program.planning = (Planning)form;
                Program.planning.Show();
            }
        }
    }
}
